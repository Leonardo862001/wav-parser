#include<stdint.h>
typedef struct head {
	uint8_t RIFF[4];
	uint32_t size;
	uint8_t wave[4];
	uint8_t fmt[4];
	uint32_t fmt_lenght;
	uint16_t format_type;
	uint16_t channels;
	uint32_t sample_rate;
	uint32_t byte_rate;
	uint16_t block_allign;
	uint16_t bits_per_sample;
	uint8_t data_chunk[4];
	uint32_t data_size;
}wav_header;
