#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include "wav.h"
#define NAME_MAX 255
FILE* file;
char* filename;
wav_header read_header(FILE* file);
void dump_data(wav_header head, FILE* file);
void find_data(FILE* file);
char* seconds_to_time(float seconds);
int main(int argc, char **argv){
	wav_header head;
	if(argc < 2){
		fprintf(stderr, "Too few arguments\n");
		exit(1);
	}
	else if(argc > 2){
		fprintf(stderr, "Too much arguments");
		exit(1);
	}
	if((filename = (char*) malloc(sizeof(char)*NAME_MAX))==NULL){
		fprintf(stderr, "Couldn't allocate memory, a\n");
		exit(1);
	}
	strcat(filename, argv[1]);
	if((file=fopen(filename, "rb"))==NULL){
		fprintf(stderr, "Couldn't allocate memory\n");
		exit(1);
	}
	head=read_header(file);
	printf("Dump samples y/N: ");
	char c = getc(stdin);
	if(c=='y')
		dump_data(head, file);
	fclose(file);
	return 0;
}

wav_header read_header(FILE* file){
	wav_header head;
	uint8_t a[4];
	uint8_t b[2];
	fread(head.RIFF, sizeof(head.RIFF), 1, file);
	printf("%c%c%c%c\n", head.RIFF[0], head.RIFF[1], head.RIFF[2], head.RIFF[3]);
	fread(a, sizeof(a), 1, file);
	head.size=a[0] | (a[1]<<8) | (a[2]<<16) | (a[3]<<24);
	printf("size: %u\n", head.size);
	fread(head.wave, sizeof(head.wave), 1, file);
	printf("%c%c%c%c\n", head.wave[0], head.wave[1], head.wave[2], head.wave[3]);
	fread(head.fmt, sizeof(head.fmt), 1, file);
	printf("%c%c%c%c\n",head.fmt[0], head.fmt[1], head.fmt[2], head.fmt[3]);
	fread(a, sizeof(a), 1, file);
	head.fmt_lenght=a[0] | (a[1]<<8) | (a[2]<<16) | (a[3]<<24);
	printf("fmt size: %u\n", head.fmt_lenght);
	fread(b, sizeof(b), 1, file);
	head.format_type=b[0] | (b[1]<<8);
	printf("format type: %u\n", head.format_type);
	fread(b, sizeof(b), 1, file);
	head.channels= b[0] | (b[1]<<8);
	printf("channels: %u\n", head.channels);
	fread(a, sizeof(a), 1, file);
	head.sample_rate=a[0] | (a[1]<<8) | (a[2]<<16) | (a[3]<<24);
	printf("sample rate: %u\n", head.sample_rate);
	fread(a, sizeof(a), 1, file);
	head.byte_rate=a[0] | (a[1]<<8) | (a[2]<<16) | (a[3]<<24);
	printf("byte rate: %u\n", head.byte_rate);
	fread(b, sizeof(b), 1, file);
	head.block_allign= b[0] | (b[1]<<8);
	printf("block allign: %u\n", head.block_allign);
	fread(b, sizeof(b), 1, file);
	head.bits_per_sample= b[0] | (b[1]<<8);
	printf("bits per sample: %u\n", head.bits_per_sample);
	find_data(file);
	head.data_chunk[0]='d',head.data_chunk[1]='a', head.data_chunk[2]='t', head.data_chunk[3]='a';
	printf("%c%c%c%c\n", head.data_chunk[0], head.data_chunk[1], head.data_chunk[2], head.data_chunk[3]);
	fread(a, sizeof(a), 1, file);
	head.data_size=a[0] | (a[1]<<8) | (a[2]<<16) | (a[3]<<24);
	printf("data size: %u\n", head.data_size);
	float duration = (float) head.size/head.byte_rate;
	printf("duration %s\n", seconds_to_time(duration));
	return head;
}
void dump_data(wav_header head, FILE* file){
	uint64_t number_of_samples=(8*head.data_size)/(head.channels * head.bits_per_sample);
	uint32_t size_of_samples =  (head.channels * head.bits_per_sample)/8;
	uint32_t sample;
	for(uint64_t i = 0; i <= number_of_samples; i++){
		fread(&sample, head.bits_per_sample/8, 1, file);
		printf("Sample %lu of %lu: %u\n", i, number_of_samples, sample);
		sample=0;
	}
}
void find_data(FILE *file){
	size_t a=1;
	uint8_t buf;
	while(a){
		do{
			fread(&buf, 1, 1, file);
		}
		while(buf!='d');
		fread(&buf, 1, 1, file);
		if(buf!='a')
			continue;
		fread(&buf, 1, 1, file);
		if(buf!='t')
			continue;
		fread(&buf, 1, 1, file);
		if(buf!='a')
			continue;
		else
			a=0;
	}
}
char* seconds_to_time(float seconds){
	char* time=(char*)malloc(sizeof(char)*10);
	int mm= seconds/60;
	int hh= seconds/3600;
	mm-=3600*hh;
	int ss= seconds-60*mm-3600*hh;
	sprintf(time,"%02d:%02d:%02d",hh, mm, ss);
	return time;
}
